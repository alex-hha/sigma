1. Create New Item -> Maven project, Item name: sigma-t3
2. Under Source Code Management section select "Git" and add repository url: https://github.com/justinjmoses/flexmojos-introduction/
3. Under Build section define Root POM: 1-Simple/pom.xml, Goals and options: compile
4. Under Build section in Advanced subsection check option - "Disable automatic artifact archiving"
5. In Post-build Actions section add "Archive the artifacts" action and define Files to archive: 1-Simple/target/*.swf
6. Build the project

P.S.
archived build artifacts: flexmojos-simplist-1.0.0-SNAPSHOT.swf
